﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RoadDelete : MonoBehaviour {

    public Transform Car;
    int SpawnDistance = 20;
    int SpawnPos;

    void Start () {
        CarMove _Car = FindObjectOfType(typeof(CarMove)) as CarMove;
        Car = _Car.transform;
    }
	
	void Update ()
    {
        if (Car.transform.position.x < transform.position.x - (SpawnDistance * 2))
        {
            Destroy(this.gameObject);
        }

    }
}
