﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class UIControl : MonoBehaviour {

    public GameObject GameOver;
    public Text CurrentScore;
    public Text BestScore;
    int Distance;
    public GameObject Game;
    public GameObject GameUI;
    public GameObject Menu;

    void Start () {
        GameOver.SetActive(false);

        if (!PlayerPrefs.HasKey("BestScore"))
            PlayerPrefs.SetInt("BestScore", 0);

        BestScore.text = "BEST : " + PlayerPrefs.GetInt("BestScore").ToString();

        if (Data.FirstStart)
        {
            Data.FirstStart = false;
            Game.SetActive(false);
            GameUI.SetActive(false);
            Menu.SetActive(true);

        }
        else
        {
            Game.SetActive(true);
            GameUI.SetActive(true);
            Menu.SetActive(false);
        }
        
    }
	

	void Update () {
        if (Data.GameOver)
        {
             if(Distance > PlayerPrefs.GetInt("BestScore"))
                PlayerPrefs.SetInt("BestScore", Distance);

            GameOver.SetActive(true);          
        }
            
        Distance = Mathf.Abs((int)Data.CarPositionX);

        CurrentScore.text = "DISTANCE : " + Distance.ToString();
    }

    public void Restart()
    {
        Application.LoadLevel(Application.loadedLevel);
        Data.GameOver = false;
    }

    public void Play()
    {
        Game.SetActive(true);
        GameUI.SetActive(true);
        Menu.SetActive(false);
        Restart();
    }

    public void OpenMenu()
    {
        Game.SetActive(false);
        GameUI.SetActive(false);
        Menu.SetActive(true);
        Data.FirstStart = true;
        Restart();
    }

}
