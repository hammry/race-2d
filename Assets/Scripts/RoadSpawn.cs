﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RoadSpawn : MonoBehaviour {

    public GameObject[] Road;
    public Transform Car;
    public int SpawnDistance = 20;
    int SpawnPos;


	void Start () {
        CarMove _Car = FindObjectOfType(typeof(CarMove)) as CarMove;
        Car = _Car.transform;

    }
	

	void Update () {

        if(Car.transform.position.x < SpawnPos + (SpawnDistance * 2))
        {
            SpawnPos = SpawnPos - SpawnDistance;

            int RoadNum = Random.Range(0, Road.Length);

            GameObject NewRoad1 = Instantiate(Road[RoadNum], new Vector3(SpawnPos,
                Road[RoadNum].transform.position.y, Road[RoadNum].transform.position.z),
                Road[RoadNum].transform.rotation);
        }

		
	}
}
