﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CarMove : MonoBehaviour {

    Transform Car;
    public float Speed = 0.5f;
    public float TurnSpeed = 0.25f;
    bool isTurning;
    bool isCarsh;

    public float LeftZone;
    public float RightZone;

    public AudioSource CarMoveSound;
    public AudioSource CarTurnSound;
    public AudioSource CarCrashSound;

    bool LeftTurn;
    bool RightTurn;


    void Start () {
        Car = this.gameObject.GetComponent<Transform>();
        isCarsh = false;

        LeftTurn = false;
        RightTurn = false;

    }
	
	
	void FixedUpdate ()
    {
        
        Car.transform.position = new Vector3(Car.transform.position.x - Speed,
            Car.transform.position.y, Car.transform.position.z);

        Data.CarPositionX = Car.transform.position.x;

        Speed = Speed * 1.0001f;
        if (Data.GameOver)
        {
            Speed = 0;
        }
        else
        {
            if (Input.GetKey(KeyCode.LeftArrow) || Input.GetKey(KeyCode.A) || LeftTurn)
            {
                isTurning = true;
                Car.transform.position = new Vector3(Car.transform.position.x, Car.transform.position.y, Car.transform.position.z - TurnSpeed);
            }


            if (Input.GetKey(KeyCode.RightAlt) || Input.GetKey(KeyCode.D) || RightTurn)
            {
                isTurning = true;
                Car.transform.position = new Vector3(Car.transform.position.x, Car.transform.position.y, Car.transform.position.z + TurnSpeed);
            }
        }
            

       


        if(Car.transform.position.z < LeftZone)
        {
            Car.transform.position = new Vector3(Car.transform.position.x, Car.transform.position.y, LeftZone);
        }

        if (Car.transform.position.z > RightZone)
        {
            Car.transform.position = new Vector3(Car.transform.position.x, Car.transform.position.y, RightZone);
        }

        Sounds();
    }

    private void OnTriggerEnter(Collider other)
    {
        Data.GameOver = true;
        Debug.Log("COLISSIN");
    }



    void Sounds()
    {

        if (!CarMoveSound.isPlaying)
            CarMoveSound.Play();

        CarMoveSound.pitch = Speed;

        if (isTurning)
        {
            if (!CarTurnSound.isPlaying)
                CarTurnSound.Play();
        }
        else
            CarTurnSound.Stop();


        isTurning = false;

        if (Data.GameOver && !isCarsh)
        {
                CarCrashSound.Play();
            isCarsh = true;

        }

        

    }

    public void TurnLeftOn()
    {
        LeftTurn = true;
    }

    public void TurnLeftOff()
    {
        LeftTurn = false;
    }

    public void TurnRightOn()
    {
        RightTurn = true;
    }

    public void TurnRightOff()
    {
        RightTurn = false;
    }


}
